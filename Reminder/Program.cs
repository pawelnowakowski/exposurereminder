﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using System.Net;

namespace Reminder
{
    public class UserMessageItem {
        public string FormName {get; set;}
        public string Message { get; set; }
        public int MessageId { get; set; }
        public int MessagePriority { get; set; } 
        public int CreatorId { get; set; }
        public string CreatorLogin { get; set; }
        public int UserId { get; set; }
        public string UserLogin {get; set;}
        public string UserFirstname {get; set;}
        public string UserLastname {get; set;}
        public string UserEmail {get; set;}
        public string UserLanguage { get; set; }
        public bool UserMailPriority { get; set; }
    }

    public class Reminder
    {
        IList<UserMessageItem> userMessageList = new List<UserMessageItem>();
        SqlConnection connection = null;

        static void Main(string[] args)
        {
            //Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine("Start aplikacji reminder");
            try
            {
                Reminder reminder = new Reminder();

                //Przelicz dni robocze na konkretna date
                int monthDay = DateTime.Now.Day;
                int monthWorkDay = DateTime.Now.Day;

                //Jesli dzisiaj jest swieto, nie wysylaj maila, bo poszedl wczoraj.
                //if (IsHoliday(DateTime.Now.Date))
                    //return;

                for (int index = monthDay; index > 0; index--)
                {
                    if (IsHoliday(new DateTime(DateTime.Now.Year, DateTime.Now.Month, index)))
                        monthWorkDay--;
                }

                reminder.createConnection();
                IList<UserMessageItem> userMessageList = reminder.getAlerts(monthWorkDay);
                reminder.createAndSendMailMessage(userMessageList);
                reminder.disposeConnection();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);

                if(ex.InnerException != null) {
                    Console.WriteLine(ex.InnerException.Message);
                    Console.WriteLine(ex.InnerException.StackTrace);
                }
            }
            Console.WriteLine("Koniec dzialanie aplikacji reminder");
        }

        public void createConnection()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["Exposure"].ToString());
            connection.Open();
            Console.WriteLine("Nawiązano połączenie z bazą danych");
        }

        public void disposeConnection()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
                Console.WriteLine("Zamknięto połączenie z bazą danych");
            }
        }

        public IList<UserMessageItem> getAlerts (int monthWorkDay) {
            string commandText = "SELECT DISTINCT form_name, message, message_id, message_priority, creator_id, creator_login, user_id, user_login, user_firstname, user_lastname, user_email, user_language, user_mail_priority FROM getAbsentForms (@monthWorkDay) GROUP BY form_name, message, message_id, message_priority, creator_id, creator_login, user_id, user_login, user_firstname, user_lastname, user_email, user_language, user_mail_priority";

            using (SqlCommand command = new SqlCommand(commandText, connection))
            {
                command.Parameters.AddWithValue("@monthWorkDay", monthWorkDay);
                
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        UserMessageItem userMessageItem = new UserMessageItem
                        {
                            FormName = reader["form_name"] == null ? null : reader["form_name"].ToString(),
                            Message = reader["message"] == null ? null : reader["message"].ToString(),
                            MessageId = int.Parse(reader["message_id"].ToString()),
                            CreatorId = int.Parse(reader["creator_id"].ToString()),
                            CreatorLogin = reader["creator_login"].ToString(),
                            UserId = int.Parse(reader["user_id"].ToString()),
                            UserLogin = reader["user_login"] == null ? null : reader["user_login"].ToString(),
                            UserFirstname = reader["user_firstname"] == null ? null : reader["user_firstname"].ToString(),
                            UserLastname = reader["user_lastname"] == null ? null : reader["user_lastname"].ToString(),
                            UserEmail = reader["user_email"] == null ? null : reader["user_email"].ToString(),
                            UserLanguage = reader["user_language"] == null ? null : reader["user_language"].ToString(),
                            UserMailPriority = Convert.ToBoolean (reader["user_mail_priority"])
                        };

                        var messagePriority = reader["message_priority"].ToString().Trim();

                        switch (messagePriority) {
                            case "Normal":
                                userMessageItem.MessagePriority = 0;
                                break;
                            case "Warning":
                                userMessageItem.MessagePriority = 1;
                                break;
                            case "Critical":
                                userMessageItem.MessagePriority = 2;
                                break;
                        }

                        userMessageList.Add(userMessageItem);
                    }
                }
            }

            if (userMessageList.Count == 0)
                Console.WriteLine("Zapytanie nie zwróciło rekordów");
            else
                Console.WriteLine("Zapytanie zwrocilo {0} rekordow", userMessageList.Count());
            return userMessageList;
        }

        public void saveAlertReciver(UserMessageItem userMessageItem)
        {
            string commandText = "INSERT INTO MessageReceivers (Created, Modified, CreatedDate, ModifiedDate, User_Id, Message_Id) VALUES (@Created, NULL, GETDATE (), NULL, @UserId, @MessageId)";

            using (SqlCommand command = new SqlCommand(commandText, connection))
            {
                command.Parameters.AddWithValue("@Created", userMessageItem.CreatorLogin);
                command.Parameters.AddWithValue("@MessageId", userMessageItem.MessageId);
                command.Parameters.AddWithValue("@UserId", userMessageItem.UserId);

                command.ExecuteNonQuery();
            }

            Console.WriteLine("Dodano użytkownika '{0}' do adresatów wiadomości '{1}'", userMessageItem.UserId, userMessageItem.MessageId);
        }

        public void saveAlert(UserMessageItem userMessageItem) {
            string commandText = "INSERT INTO Messages (Priority, Subject, [Content], SendDate, Created, Modified, CreatedDate, ModifiedDate, Sender_Id) SELECT (CASE Priority WHEN 'Warning' THEN 1 WHEN 'Critical' THEN 2 ELSE 0 END), [Content], [Content], GETDATE (), Created, NULL, GETDATE (), NULL, @SenderId FROM FormAlerts WHERE Id = @AlertId; SELECT SCOPE_IDENTITY()";

            using (SqlCommand command = new SqlCommand(commandText, connection))
            {
                command.Parameters.AddWithValue("@AlertId", userMessageItem.MessageId);
                command.Parameters.AddWithValue("@SenderId", userMessageItem.CreatorId);

                userMessageItem.MessageId = int.Parse(command.ExecuteScalar().ToString());
            }

            Console.WriteLine("Wiadomość '{0}' została zapisana pod identyfikatorem '{1}'", userMessageItem.Message, userMessageItem.MessageId);
        }

        public void createAndSendMailMessage(IList<UserMessageItem> userMessageList)
        {
            SmtpClient client = new SmtpClient();
            MailMessage message = new MailMessage();
            string smtpPortString = ConfigurationManager.AppSettings["MailServerPort"];
            string smtpHost = ConfigurationManager.AppSettings["MailServerHost"] ?? String.Empty;
            string smtpSslString = ConfigurationManager.AppSettings["MailServerSsl"] ?? String.Empty;
            int smtpPort = 0;
            bool smtpSsl = false;

            //Nazwa serwera SMTP jest wymagana
            if (smtpHost == String.Empty)
            {
                Console.WriteLine("Nazwa / adres serwera SMTP jest wymagany");
                return;
            }
            //Port musi byc liczba
            if (smtpPortString != String.Empty && !int.TryParse(smtpPortString, out smtpPort))
            {
                Console.WriteLine("Port serwera SMTP musi byc liczbą");
                return;
            }
            else
                smtpPort = 25;
            //czy serwer Ssl
            if (smtpSslString != String.Empty && !bool.TryParse(smtpSslString, out smtpSsl))
            {
                Console.WriteLine("Czy serwer SSL musi być boolean");
                return;
            }

            client.DeliveryFormat = SmtpDeliveryFormat.International;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Host = smtpHost ?? String.Empty;
            client.Port = smtpPort;
            client.EnableSsl = smtpSsl;
            client.UseDefaultCredentials = true;

            message.From = new MailAddress(ConfigurationManager.AppSettings["SystemEmail"] ?? String.Empty, ConfigurationManager.AppSettings["SystemName"] ?? String.Empty);
            message.IsBodyHtml = true;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.SubjectEncoding = System.Text.Encoding.UTF8;

            //AlternateView plainTextView = AlternateView.CreateAlternateViewFromString(body.Trim(), new ContentType("text/html; charset=UTF-8"));
            //plainTextView.TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable;
            //message.AlternateViews.Add(plainTextView);

            UserMessageItem lastUserMessageItem = null;
            bool isNewSection = true;

            for (int index = 0; index < userMessageList.Count; index++)
            {
                UserMessageItem userMessageItem = userMessageList[index];
                isNewSection = lastUserMessageItem == null || lastUserMessageItem.FormName != userMessageItem.FormName || lastUserMessageItem.UserLanguage != userMessageItem.UserLanguage;

                //Warunek dla pierwszego rekordu
                if (isNewSection)
                {
                    saveAlert(userMessageItem);

                    if (lastUserMessageItem != null)
                    {
                        if (message.To.Count() > 0)
                            sendMessage(lastUserMessageItem, message, client);

                        message.To.Clear();
                    }

                    lastUserMessageItem = userMessageItem;
                }
                
                MailAddress userMailAddress = new MailAddress(userMessageItem.UserEmail, String.Concat(userMessageItem.UserFirstname, " ", userMessageItem.UserLastname));
                if (!message.To.Contains(userMailAddress)) 
                {
                    //Wysylaj maila tylko jesli status > 'Normal' lub adresat chce otrzymywac wszystkie maile
                    if (userMessageItem.MessagePriority.Equals("Warning") || userMessageItem.MessagePriority.Equals("Critical") || userMessageItem.UserMailPriority)
                        message.To.Add(userMailAddress);

                    userMessageItem.MessageId = lastUserMessageItem.MessageId;
                    saveAlertReciver(userMessageItem);
                }
            }

            //Jesli zapytanie zwrocilo rekordy, wyslij ostatni mail
            if (lastUserMessageItem != null)
                sendMessage(lastUserMessageItem, message, client);
        }

        public void sendMessage(UserMessageItem userMessageItem, MailMessage message, SmtpClient client)
        {
            var userEmailArray = message.To.Select(ma => ma.Address).ToArray();
            Console.WriteLine("Wysyłam mail do użytkownika / użytkowników {0} z informacją: '{1}'", String.Join(",", userEmailArray), userMessageItem.Message);
            message.Subject = userMessageItem.FormName;

            string emailText = userMessageItem.Message;
            string appConfigKey = String.Concat(userMessageItem.FormName.ToLower(), "Text");
            if (ConfigurationManager.AppSettings.AllKeys.Contains(appConfigKey))
                emailText = String.Concat(emailText, ConfigurationManager.AppSettings[appConfigKey]);

            message.Body = emailText;

            if (ConfigurationManager.AppSettings.AllKeys.Contains("SystemPassword"))
                client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SystemEmail"], ConfigurationManager.AppSettings["SystemPassword"]);

            client.Send(message);
        }

        public static bool IsHoliday(DateTime day)
        {
            if (day.DayOfWeek == DayOfWeek.Saturday || day.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }

            if (day.Month == 1 && day.Day == 1)
            {
                return true; // Nowy Rok
            }

            if (day.Month == 1 && day.Day == 6)
            {
                return true; // Trzech Króli
            }

            if (day.Month == 5 && day.Day == 1)
            {
                return true; // 1 maja
            }

            if (day.Month == 5 && day.Day == 3)
            {
                return true; // 3 maja
            }

            if (day.Month == 8 && day.Day == 15)
            {
                return true; // Wniebowzięcie Najświętszej Marii Panny, Święto Wojska Polskiego
            }

            if (day.Month == 11 && day.Day == 1)
            {
                return true; // Dzień Wszystkich Świętych
            }

            if (day.Month == 11 && day.Day == 11)
            {
                return true; // Dzień Niepodległości 
            }

            if (day.Month == 12 && day.Day == 25)
            {
                return true; // Boże Narodzenie
            }

            if (day.Month == 12 && day.Day == 26)
            {
                return true; // Boże Narodzenie
            }

            int a = day.Year % 19;
            int b = day.Year % 4;
            int c = day.Year % 7;
            int d = ((a * 19) + 24) % 30;
            int e = ((2 * b) + (4 * c) + (6 * d) + 5) % 7;

            if (d == 29 && e == 6)
            {
                d -= 7;
            }

            if (d == 28 && e == 6 && a > 10)
            {
                d -= 7;
            }

            DateTime easter = new DateTime(day.Year, 3, 22).AddDays(d + e);

            if (day.AddDays(-1) == easter)
            {
                return true; // Wielkanoc (poniedziałek)
            }

            if (day.AddDays(-60) == easter)
            {
                return true; // Boże Ciało
            }

            return false;
        }
    }
}
